from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from . import models
from .forms import PacienteForm, AgendaForm
import datetime
# Create your views here.


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse('core:index'))


@login_required
def index(request):
    """Página inicial"""
    agenda = models.Agenda.objects.all()
    for var in agenda:
        a = var.observacoes.replace("\n", "").replace('\r', '')
        var.observacoes = a

    context = {'agenda': agenda}
    return render(request, 'core/index.html', context)


@login_required
def adicionaPaciente(request):
    """Adiciona um novo paciente"""
    if request.method != 'POST':
        # Nenhum dado submetido, cria o formulário em branco
        form = PacienteForm
    else:
        # Caso o formulário venha preenchido os dados precisam ser salvos no banco
        form = PacienteForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('core:pacientes'))
    context = {'form': form}
    return render(request, 'core/adicionaPaciente.html', context)

@login_required
def pacientes(request):
    """Mostra lista de pacientes do consultorio"""
    pacientes = models.Paciente.objects.all()
    context = {'pacientes': pacientes}
    return render(request, 'core/pacientes.html', context)

@login_required
def editPaciente(request, paciente_id):
    """Recebe um paciente para edição"""
    paciente = models.Paciente.objects.get(id=paciente_id)
    if request.method != 'POST':
        form = PacienteForm(instance=paciente)
    else:
        # Processa dados para edição de paciente
        form = PacienteForm(instance=paciente, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('core:pacientes'))
    context = {'form': form, 'paciente': paciente}
    return render(request, 'core/editaPaciente.html', context)

@login_required
def novoAgendamento(request):
    """Cadastra novos agendamentos"""
    if request.method != 'POST':
        form = AgendaForm
    else:
        form = AgendaForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('core:index'))
    context = {'form': form}
    return render(request, 'core/cadastraConsulta.html', context)

@login_required
def editaAgendamento(request, agenda_id):
    """Edita um agendamento de consulta"""
    agenda = models.Agenda.objects.get(id=agenda_id)
    if request.method != 'POST':
        form = AgendaForm(instance=agenda)
    else:
        form = AgendaForm(instance=agenda, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('core:index'))
    context = {'form': form, 'agenda': agenda}
    return render(request, 'core/editaConsulta.html', context)

@login_required
def historicoPaciente(request, paciente_id):
    """Retorna um histórico do paciente selecionado"""
    paciente = models.Paciente.objects.get(id=paciente_id)
    historico = models.Agenda.objects.filter(paciente=paciente_id, data_consulta__lte=datetime.datetime.now())
    # Verifica se o pacientee realizou algum consulta e não pagou, ou pagou e a consulta não foi realizada.
    saldo = 0;
    for hist in historico:
        if hist.status.id == 2 and hist.form_pagamento is None:
           saldo += hist.servico.valor
        elif hist.status.id == 1 and hist.form_pagamento is not None:
            saldo -= hist.servico.valor
    context = {'historico': historico, 'paciente': paciente, 'saldo': saldo}
    return render(request, 'core/historico.html', context)

