from django.db import models
from django.contrib import admin
from django.utils.timezone import now
import datetime
# Create your models here.


class FormPagamento(models.Model):
    """Modelo que irá criar as formas de pagamento"""
    forma = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Formas de Pagamento"

    def __str__(self):
        return self.forma


class StatusConsulta(models.Model):
    """Modelo que irá criar os status da consulta Realizada/Cancelada/Remarcada"""
    status = models.CharField(max_length=50)

    def __str__(self):
        return self.status


class Servico(models.Model):
    """Modelo que irá cadastrar os tipos de serviços oferecidos pela clinica"""
    servico = models.CharField(max_length=200)
    valor = models.DecimalField(max_digits=8, decimal_places=2)
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.servico


class ServicoAdmin(admin.ModelAdmin):
    """ Classe que define a forma que os serviços serão apresentados no site de administração """
    list_display = ('servico', 'valor', 'data_criacao')


class Paciente(models.Model):
    """Modelo que irá Criar os Pacientes da Clinica"""

    nome = models.CharField(max_length=250)
    nascimento = models.DateField(null=True, blank=True)
    cpf = models.CharField(max_length=15, null=True, blank=True)
    profissao = models.CharField(max_length=250, null=True, blank=True)
    celular = models.CharField(max_length=20)
    email = models.CharField(max_length=100, null=True, blank=True)
    cep = models.CharField(max_length=15, null=True, blank=True)
    endereco = models.CharField(max_length=250, null=True, blank=True)
    cidade = models.CharField(max_length=100, null=True, blank=True)
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nome


class PacienteAdmin(admin.ModelAdmin):
    """Classe que define como o modelo será mostrado no site de administração do django"""
    list_display = ('nome', 'email', 'cpf', 'data_criacao')


class Agenda(models.Model):
    """Criação da agenda de consultas da Clinica"""
    data_consulta = models.DateField()
    hora_inicio = models.TimeField()
    hora_final = models.TimeField()
    paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    servico = models.ForeignKey(Servico, on_delete=models.DO_NOTHING)
    observacoes = models.TextField()
    status = models.ForeignKey(StatusConsulta, null=True, blank=True, on_delete=models.DO_NOTHING, default=1)
    form_pagamento = models.ForeignKey(FormPagamento, null=True, blank=True, on_delete=models.DO_NOTHING)
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.servico.servico

    class Meta:
        verbose_name = "Agenda"


class AgendaAdmin(admin.ModelAdmin):
    """Classe que define como as informações das consultas serão mostradas no painel ADM"""
    list_display = ('data_consulta', 'paciente', 'servico', 'status')
