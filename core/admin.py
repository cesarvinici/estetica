from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(FormPagamento)
admin.site.register(StatusConsulta)
admin.site.register(Paciente, PacienteAdmin)
admin.site.register(Servico, ServicoAdmin)
admin.site.register(Agenda, AgendaAdmin)