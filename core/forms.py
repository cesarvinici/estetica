from django import forms
from .models import Paciente, Agenda


class DateInput(forms.DateInput):
    input_type = 'datetime-local'


class PacienteForm(forms.ModelForm):
    class Meta:
        model = Paciente
        fields = ['nome', 'nascimento', 'cpf', 'profissao', 'celular', 'email', 'cep', 'endereco', 'cidade']

class AgendaForm(forms.ModelForm):
    class Meta:
        model = Agenda
        fields = ['paciente', 'servico', 'data_consulta', 'hora_inicio', 'hora_final', 'observacoes', 'status', 'form_pagamento']
        labels = {'paciente': 'Paciente', 'servico': 'Serviço', 'data_consulta': 'Data da Consulta',
                  'hora_inicio': 'Horario de Início', 'hora_final': 'Horario Final',
                  'observacoes': 'Observações', 'status': 'Status', 'form_pagamento': 'Forma de Pagamento'}