from django.urls import path
from django.contrib.auth.views import login
from . import views


app_name = 'core'
urlpatterns = [
    # ''
    path('', views.index, name='index'),
    path('adicionar-paciente', views.adicionaPaciente, name="adicionaPaciente"),
    path('pacientes', views.pacientes, name='pacientes'),
    path('paciente/<paciente_id>', views.editPaciente, name='editaPaciente'),
    path('agendamento', views.novoAgendamento, name='novoAgendamento'),
    path('editar-agendamento/<agenda_id>', views.editaAgendamento, name="editarAgendamento"),
    path('historico/<paciente_id>', views.historicoPaciente, name='historicoPaciente'),
    path('login/', login, {'template_name': 'core/login.html'}, name='login'),
    path('logout', views.logout_user, name='logout_page')

]
